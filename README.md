requirement OS centos7

step:

1. yum -y install wget gunzip nano

2. wget https://gitlab.com/febrian.nice/centos7-chr/-/raw/main/chr-install.sh?inline=false -O chr-install.sh

3. chmod +x chr-install.sh

4. nano chr-install.sh, edit interface and disk

5. sh chr-install.sh

6. reboot
