#note:
#sesuaikan interface ens33
#sesuaikan disk /dev/sda
#========start=======
wget https://download.mikrotik.com/routeros/6.49.2/chr-6.49.2.img.zip -O chr.img.zip  && \
gunzip -c chr.img.zip > chr.img  && \
mount -o loop,offset=512 chr.img /mnt && \
ADDRESS=`ip addr show ens33 | grep global | cut -d' ' -f 6 | head -n 1` && \
GATEWAY=`ip route list | grep default | cut -d' ' -f 3` && \
echo "/ip address add address=$ADDRESS interface=[/interface ethernet find where name=ether1]
/ip route add gateway=$GATEWAY
/ip dns set servers=8.8.8.8,8.8.4.4" > /mnt/rw/autorun.scr && \
umount /mnt && \
echo u > /proc/sysrq-trigger && \
dd if=chr.img bs=1024 of=/dev/sda && \
echo "sync disk" && \
echo s > /proc/sysrq-trigger && \
echo "Sleep 6 seconds" && \
sleep 6 && \
echo "Ok, reboot" && \
echo b > /proc/sysrq-trigger
#=========end========
